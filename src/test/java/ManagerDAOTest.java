import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.jarekkozmic.crudapp.config.HibernateUtil;
import pl.jarekkozmic.crudapp.config.SpringUtil;
import pl.jarekkozmic.crudapp.dao.ManagerDAO;
import pl.jarekkozmic.crudapp.entity.Department;
import pl.jarekkozmic.crudapp.entity.Manager;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

public class ManagerDAOTest {

    private static ManagerDAO managerDAO;
    private static HibernateUtil hibernateUtil;
    private static Properties properties;

    @BeforeAll
    public static void setUp(){
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringUtil.class);
        InputStream inputStream = ManagerDAOTest.class.getResourceAsStream("/META-INF/hibernate.properties");
        properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        hibernateUtil = (HibernateUtil) context.getBean("hibernateUtil");
        hibernateUtil.changeUrl(properties.getProperty("testUrl"));
        managerDAO = (ManagerDAO) context.getBean("managerDAO");
        managerDAO.setFactory(hibernateUtil.getFactory());
    }

    @Test
    protected void managerDAOTest(){
        Manager manager = new Manager("managerNameTest", "managerSurnameTest", null);
        int id = managerDAO.create(manager);
        assertEquals(manager.getName(), managerDAO.getById(id).getName());

        assertNotNull(managerDAO.read());

        manager.setName("justTest");
        manager.setSurname("justTest");
        manager.setDepartment( new Department("testDepartment"));
        managerDAO.update(manager);

        assertEquals(manager.getName(), managerDAO.getById(id).getName());
        assertEquals(manager.getSurname(), managerDAO.getById(id).getSurname());
        assertEquals(manager.getDepartment().getName(), managerDAO.getById(id).getDepartment().getName());

        managerDAO.delete(manager);
        assertThrows(NoResultException.class, ()->managerDAO.getById(id));

    }

    @AfterAll
    public static void tearDown(){
        hibernateUtil.changeUrl(properties.getProperty("url"));
        managerDAO.setFactory(hibernateUtil.getFactory());
    }
}
