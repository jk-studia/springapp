import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.jarekkozmic.crudapp.config.HibernateUtil;
import pl.jarekkozmic.crudapp.config.SpringUtil;
import pl.jarekkozmic.crudapp.dao.DepartmentDAO;
import pl.jarekkozmic.crudapp.entity.Department;

import javax.persistence.NoResultException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DepartmentDAOTest {

    private static DepartmentDAO departmentDAO;
    private static HibernateUtil hibernateUtil;
    private static Properties properties;

    @BeforeAll
    public static void setUp(){
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringUtil.class);
        InputStream inputStream = DepartmentDAOTest.class.getResourceAsStream("/META-INF/hibernate.properties");
        properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        hibernateUtil = (HibernateUtil) context.getBean("hibernateUtil");
        hibernateUtil.changeUrl(properties.getProperty("testUrl"));
        departmentDAO = (DepartmentDAO) context.getBean("departmentDAO");
        departmentDAO.setFactory(hibernateUtil.getFactory());
    }

    @Test
    public void departmentDAOTest(){
        Department department = new Department("departmentTest");
        int id = departmentDAO.create(department);
        assertEquals(department.getName(), departmentDAO.getById(id).getName());

        assertNotNull(departmentDAO.read());

        department.setName("justTest");
        departmentDAO.update(department);
        assertEquals(department.getName(), departmentDAO.getById(id).getName());

        departmentDAO.delete(department);
        assertThrows(NoResultException.class, ()->departmentDAO.getById(id));

    }

    @AfterAll
    public static void tearDown(){
        hibernateUtil.changeUrl(properties.getProperty("url"));
        departmentDAO.setFactory(hibernateUtil.getFactory());
    }
}
