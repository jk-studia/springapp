package pl.jarekkozmic.crudapp.entity;

import pl.jarekkozmic.crudapp.api.IPerson;

import javax.persistence.*;

@Entity
@Table(name = "manager")
public class Manager implements IPerson {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "managerId", nullable = false, unique = true)
    private int id;

    @Column(name = "name", length = 45, nullable = false)
    private String name;

    @Column(name = "surname", length = 45, nullable = false)
    private String surname;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "departmentId", referencedColumnName = "departmentId")
    private Department department;

    public Manager() {
    }

    public Manager(String name, String surname, Department department) {
        this.name = name;
        this.surname = surname;
        this.department = department;
    }

    public int getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public void introduceYourself() {
        System.out.println("Hello, my name is "+name+" "+surname+". I'm manager of "+department+" department.");
    }
}
