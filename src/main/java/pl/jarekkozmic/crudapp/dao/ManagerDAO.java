package pl.jarekkozmic.crudapp.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.jarekkozmic.crudapp.api.IDAO;
import pl.jarekkozmic.crudapp.entity.Manager;

import javax.persistence.Query;
import java.util.List;

public class ManagerDAO implements IDAO {

    private SessionFactory factory;
    private List managers;
    public ManagerDAO(SessionFactory factory){
        this.factory = factory;
    }

    public void setFactory(SessionFactory factory){
        this.factory = factory;
    }

    @Override
    public int create(Object object) {
        Manager manager = (Manager) object;
        customTransaction(manager, 1);
        return manager.getId();
    }

    @Override
    public List read() {
        managers = null;
        customTransaction(null, 2);
        return managers;
    }

    @Override
    public void update(Object object) {
        Manager manager = (Manager) object;
        customTransaction(manager, 3);
    }

    @Override
    public void delete(Object object) {
        Manager manager = (Manager) object;
        customTransaction(manager, 4);
    }

    public Manager getById(int id){
        Transaction transaction = null;
        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Manager where id = :id");
            query.setParameter("id", id);
            transaction.commit();
            return (Manager) query.getSingleResult();
        } catch (HibernateException e) {
            if(transaction!=null)
                transaction.rollback();
            System.out.println("Manager get failed");
            return null;
        }
    }


    private void customTransaction(Manager manager, int operationNumber){
        Transaction transaction = null;
        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            switch (operationNumber){
                case 1: if(manager!=null) session.save(manager); break;
                case 2: managers = session.createQuery("from Manager").list(); break;
                case 3: if(manager!=null) session.update(manager); break;
                case 4: if(manager!=null) session.delete(manager); break;
            }
            transaction.commit();
        } catch (HibernateException e) {
            if(transaction!=null)
                transaction.rollback();
            e.printStackTrace();
        }
    }
}
