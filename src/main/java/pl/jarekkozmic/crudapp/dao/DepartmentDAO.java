package pl.jarekkozmic.crudapp.dao;

import org.hibernate.*;
import pl.jarekkozmic.crudapp.api.IDAO;
import pl.jarekkozmic.crudapp.entity.Department;

import javax.persistence.Query;
import java.util.List;


public class DepartmentDAO implements IDAO {

    private SessionFactory factory;
    private List departments;
    public DepartmentDAO(SessionFactory factory){
        this.factory = factory;
    }

    public void setFactory(SessionFactory factory){
        this.factory = factory;
    }


    @Override
    public int create(Object object) {
        Department department = (Department) object;
        customTransaction(department, 1);
        return department.getId();
    }

    @Override
    public List read() {
        departments = null;
        customTransaction(null, 2);
        return departments;
    }

    @Override
    public void update(Object object) {
        Department department = (Department) object;
        customTransaction(department, 3);
    }

    @Override
    public void delete(Object object) {
        Department department = (Department) object;
        customTransaction(department, 4);
    }

    public Department getById(int id){
        Transaction transaction = null;
        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Department where id = :id");
            query.setParameter("id", id);
            transaction.commit();
            return (Department) query.getSingleResult();
        } catch (HibernateException e) {
            if(transaction!=null)
                transaction.rollback();
            System.out.println("Department get failed");
            return null;
        }
    }

    private void customTransaction(Department department, int operationNumber){
        Transaction transaction = null;
        try (Session session = factory.openSession()) {
            transaction = session.beginTransaction();
            switch (operationNumber){
                case 1: if(department!=null) session.save(department); break;
                case 2: departments = session.createQuery("from Department").list(); break;
                case 3: if(department!=null) session.update(department); break;
                case 4: if(department!=null) session.delete(department); break;
            }
            transaction.commit();
        } catch (HibernateException e) {
            if(transaction!=null)
                transaction.rollback();
            e.printStackTrace();
        }
    }

}
