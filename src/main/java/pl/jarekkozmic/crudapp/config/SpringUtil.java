package pl.jarekkozmic.crudapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import pl.jarekkozmic.crudapp.dao.DepartmentDAO;
import pl.jarekkozmic.crudapp.dao.ManagerDAO;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Configuration
public class SpringUtil {

    @Bean(name = "hibernateUtil")
    @Scope(scopeName = "singleton")
    public HibernateUtil hibernateUtil(){

        Properties config = new Properties();
        try (InputStream stream = getClass().getResourceAsStream("/META-INF/hibernate.properties")) {
            if(stream!=null) {
                config.load(stream);
                return new HibernateUtil(
                        config.getProperty("driver"),
                        config.getProperty("dialect"),
                        config.getProperty("url"),
                        config.getProperty("user"),
                        config.getProperty("pass"),
                        config.getProperty("hbm2ddl")
                        );
            }
            else
                throw new IOException("There are no values in property file or property file doesn't exist");

        } catch (IOException e) {
            if(e.getMessage()!=null)
                System.out.println(e.getMessage());
            System.exit(-1);
            return null;
        }
    }


    @Bean(name = "departmentDAO")
    @Scope(scopeName = "singleton")
    public DepartmentDAO departmentDAO(HibernateUtil hibernateUtil){
        return new DepartmentDAO(hibernateUtil.getFactory());
    }

    @Bean(name = "managerDAO")
    @Scope(scopeName = "singleton")
    public ManagerDAO managerDAO(HibernateUtil hibernateUtil){
        return new ManagerDAO(hibernateUtil.getFactory());
    }


}
