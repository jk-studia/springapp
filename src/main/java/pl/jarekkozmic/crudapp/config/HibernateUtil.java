package pl.jarekkozmic.crudapp.config;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import pl.jarekkozmic.crudapp.entity.Department;
import pl.jarekkozmic.crudapp.entity.Manager;

import java.util.Properties;

public class HibernateUtil {

    private SessionFactory factory;
    private Properties properties;

    public HibernateUtil(String driver, String dialect, String url, String user, String pass, String hbm2ddl){
        properties = new Properties();
        properties.put(Environment.DRIVER, driver);
        properties.put(Environment.DIALECT, dialect);
        properties.put(Environment.URL, url);
        properties.put(Environment.USER, user);
        properties.put(Environment.PASS, pass);
        properties.put(Environment.HBM2DDL_AUTO, hbm2ddl);
        factory = buildSessionFactory();
    }

    public void changeUrl(String newUrl){
        factory.close();
        properties.setProperty(Environment.URL, newUrl);
        factory = buildSessionFactory();
    }

    private SessionFactory buildSessionFactory(){
        Configuration configuration = new Configuration();
        configuration.setProperties(properties);
        configuration.addAnnotatedClass(Department.class);
        configuration.addAnnotatedClass(Manager.class);
        ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        return configuration.buildSessionFactory(registry);
    }

    public SessionFactory getFactory() {
        return factory;
    }

}
