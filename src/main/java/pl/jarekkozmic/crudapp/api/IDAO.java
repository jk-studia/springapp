package pl.jarekkozmic.crudapp.api;

import java.util.List;

public interface IDAO {
    int create(Object object);
    List read();
    void update(Object object);
    void delete(Object object);
}
