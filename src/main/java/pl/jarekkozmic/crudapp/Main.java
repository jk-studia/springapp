package pl.jarekkozmic.crudapp;


import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.jarekkozmic.crudapp.config.HibernateUtil;
import pl.jarekkozmic.crudapp.config.SpringUtil;
import pl.jarekkozmic.crudapp.dao.DepartmentDAO;
import pl.jarekkozmic.crudapp.dao.ManagerDAO;
import pl.jarekkozmic.crudapp.entity.Department;
import pl.jarekkozmic.crudapp.entity.Manager;

public class Main {
    public static void main(String[] args){
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringUtil.class);
        SessionFactory factory = ((HibernateUtil) context.getBean("hibernateUtil")).getFactory();

        DepartmentDAO departmentDAO = (DepartmentDAO) context.getBean("departmentDAO");
        ManagerDAO managerDAO = (ManagerDAO) context.getBean("managerDAO");

//        Department department = new Department("Schooling");
//        departmentDAO.create(department);
//        managerDAO.create(new Manager("Jarek", "Kozmic", department));

//        Department department = departmentDAO.getById(4);
//        departmentDAO.delete(department);


        for (Object o: departmentDAO.read()) {
            Department tmpDepartment = (Department) o;
            System.out.println(tmpDepartment.getId() + " " + tmpDepartment.getName());
        }

        System.out.println();

        for (Object o: managerDAO.read()) {
            Manager tmpManager = (Manager) o;
            System.out.println(tmpManager.getId() + " " + tmpManager.getName());
        }

        Manager manager = managerDAO.getById(1);
        manager.introduceYourself();

        factory.close();

        System.exit(0);
    }
}
