drop view if exists managers;
drop table if exists manager;
drop table if exists department;

create table department(
	departmentId int auto_increment not null unique primary key,
    departmentName varchar(45) not null
);

create table manager(
	managerId int auto_increment not null unique primary key,
	name varchar(45) not null,
    surname varchar(45) not null,
    departmentId int,
    foreign key (departmentId) references department(departmentId)
);

insert into department(departmentName)
value ("HR");

insert into department(departmentName)
value ("Sales");

insert into department(departmentName)
value ("IT");

select * from department;

insert into manager(name, surname, departmentId)
values ("Jan", "Zaremba", 1);

insert into manager(name, surname, departmentId)
values ("Natalia", "Nowak", 2);

create view managers as
select manager.surname, manager.name, department.departmentName from manager inner join department on manager.departmentId = department.departmentId order by(manager.surname); 

select * from managers;
